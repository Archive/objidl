/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/*
 *  objidl: Various OO language wrappers for libIDL
 *
 *  Copyright (C) 1998 Phil Dawes
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Phil Dawes <philipd@parallax.co.uk>
 *
 */

#ifndef CPPIDL_HH
#define CPPIDL_HH

#include <stdio.h>
#include <glib.h>
#include <libIDL/IDL.h>

class IDLTree
{
protected:
	IDL_tree m_tree;

	static const char* m_separator;
private:
  
	// This is horrible: We keep a ref to all the char*'s we
	// give out, so that they can be free'd when this object dies.
	//  list<char*> ids;

  
protected:
	IDLTree();
	IDLTree(IDL_tree tree);

	static IDLTree* tree_factory(IDL_tree tree);

	static int no_objects;
	
public:
	// copy constructor
	IDLTree::IDLTree(const IDLTree& tree);

	static int get_no_objects() {return no_objects;}

	~IDLTree();

	int get_type();
	
	IDL_tree get_raw_IDL_tree() const {return m_tree;}
		
	bool is_null();

	static IDLTree* parse_file(const char* filename);

	/** sets the string used when getting fully qualified names
	 */
	static void set_separator(const char* m_seperator);
	
};




class IDLList : public IDLTree
{
public:  
	IDLList(IDL_tree t);
	IDLList* next();
	IDLTree* get_tree();
};


class IDLIdent : public IDLTree
{
public:
	IDLIdent(IDL_tree t);
	const char*	get_name(int indent);
	const char* get_repoid();	
};



class IDLTypeDcl : public IDLTree
{
public:
	IDLTypeDcl(IDL_tree t);
	IDLTree* get_typespec();
	IDLList* get_dcls();
};



class IDLConstDcl : public IDLTree
{
public:
	IDLConstDcl(IDL_tree t);
};




class IDLExceptDcl : public IDLTree
{
public:
	IDLExceptDcl(IDL_tree t);
	const char* get_name(int indent);
	IDLList* get_contents();
};


class IDLStruct : public IDLTree
{
public:
	IDLStruct(IDL_tree t);
	const char* get_name(int indent);
	const char* get_repoid();
	IDLList* get_contents();
};


class IDLUnion : public IDLTree
{
public:
	IDLUnion(IDL_tree t);
	const char* get_name(int indent);
	const char* get_repoid();
	IDLTree* get_switch_typespec();
	IDLList* get_switch_body();
};





class IDLAttrDcl : public IDLTree
{
public:
	IDLAttrDcl(IDL_tree t);
};



class IDLOpDcl : public IDLTree
{	
public:
	IDLOpDcl(IDL_tree t);
	const char* get_name(int indent);
	IDLTree* get_return_typespec();
	IDLList* get_arguments();
	IDLList* get_exceptions();
};



class IDLParamDcl : public IDLTree
{
public:
	/*
	enum attr {
		IN,
		OUT,
		INOUT,
		RETVAL,
	};
	*/
public:
	IDLParamDcl(IDL_tree t);
	int get_attr();  // in out or inout?
	IDLTree* get_typespec();
	const char* get_name();
};


class IDLCaseStmt : public IDLTree
{
public:
	IDLCaseStmt(IDL_tree t);
};



class IDLInterface : public IDLTree
{
public:
	IDLInterface(IDL_tree t);
	const char*	get_name(int indent);
	const char* get_repoid();
	
	IDLList* get_contents();
	IDLList* get_inheritance_spec();
};

class IDLForwardDcl : public IDLTree
{
public:
	IDLForwardDcl(IDL_tree t);
};





class IDLModule : public IDLTree
{
public:
	IDLModule(IDL_tree t);
	// You must delete this string
	const char * get_name(int indent);
   	IDLList* get_contents();	
};


class IDLMember : public IDLTree
{
public:
	IDLMember(IDL_tree t);
	IDLTree* get_typespec();
	IDLList* get_dcls();
};


class IDLPrimitiveType : public IDLTree
{
public:
	IDLPrimitiveType(IDL_tree t);	
};

class IDLTypeInteger : public IDLTree
{
public:
	enum bitwidth{
		SHORT,
		LONG,
		LONGLONG,
	};
  
public:
	IDLTypeInteger(IDL_tree t);
	bitwidth get_width();
	bool is_signed();
};


class IDLTypeFloat : public IDLTree
{
public:  
	enum floattype{
		FLOAT,
		DOUBLE,
		LONGDOUBLE,
	};
public:
	IDLTypeFloat(IDL_tree t);
	floattype get_floattype();
};

class IDLTypeSequence : public IDLTree
{
public:
	IDLTypeSequence(IDL_tree t);
	IDLTree* get_simple_typespec();
	int get_size();
};

#endif
