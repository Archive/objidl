/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/*
 *  objidl: Various OO language wrappers for libIDL
 *
 *  Copyright (C) 1998 Phil Dawes
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Phil Dawes <philipd@parallax.co.uk>
 *
 */

#include "cppIDL.hh"
#include <stdlib.h>
#include <iostream>
#include <errno.h>

int IDLTree::no_objects=0;
const char* IDLTree::m_separator=0;// default set in parse_file


IDLTree*
IDLTree::parse_file(const char* filename)
{
	set_separator("::");
	IDL_tree tree;
	IDL_ns namespc;
	char* cpp_args = 0;
	int debuglevel = 0;
	int res = IDL_parse_filename(filename, cpp_args, NULL,
								 &tree, &namespc, 0, debuglevel);


	if(IDL_SUCCESS != res) {
		if (res == -1)
			g_warning("Parse of %s failed: %s", filename, g_strerror(errno));
		else
			g_warning("Parse of %s failed", filename);
		exit(0);
	}

	//	cout << "tree = " << tree << endl;
	
	IDLTree* t = tree_factory(tree);
	return t;
}

void
IDLTree::set_separator(const char* separator)
{
	// free the old string
	g_free((void*)m_separator);
	// and create a new one
	m_separator = g_strdup(separator);
}

IDLTree*
IDLTree::tree_factory(IDL_tree tree)
{
	switch(IDL_NODE_TYPE(tree)) {
	case IDLN_ATTR_DCL:
		g_error("objIDL doesn't support attr types yet");
		g_assert(false);
		break;
	case IDLN_BINOP:
		g_error("objIDL doesn't support binop types yet");
		g_assert(false);
		break;
	case IDLN_BOOLEAN:
		g_error("objIDL doesn't support boolean types yet");		
		g_assert(false);
		break;
	case IDLN_CASE_STMT:
		g_error("objIDL doesn't support stmt types yet");		
		g_assert(false);
		break;
	case IDLN_CHAR:
		g_error("objIDL doesn't support char types yet");		
		g_assert(false);
		break;
	case IDLN_CONST_DCL:
		g_error("objIDL doesn't support const dcl types yet");		
		g_assert(false);
		break;
	case IDLN_EXCEPT_DCL:
		return new IDLExceptDcl(tree);
		break;
	case IDLN_FIXED:
		g_error("objIDL doesn't support fixed types yet");		
		g_assert(false);
		break;
	case IDLN_FLOAT:
		g_error("objIDL doesn't support float types yet");		
		g_assert(false);
		break;
	case IDLN_FORWARD_DCL:
		g_error("objIDL doesn't support forward dcl types yet");		
		g_assert(false);
		break;
	case IDLN_GENTREE:
		g_error("objIDL doesn't support err.. gentree types yet");		
		g_assert(false);
		break;
	case IDLN_IDENT:
		return new IDLIdent(tree);
		break;
	case IDLN_INTEGER:
		g_error("objIDL doesn't support integers yet");		
		g_assert(false);
		break;
	case IDLN_INTERFACE:
		return new IDLInterface(tree);
		break;
	case IDLN_LIST:
		return new IDLList(tree);
		break;
	case IDLN_MEMBER:
		return new IDLMember(tree);
		break;
	case IDLN_MODULE:
		return new IDLModule(tree);
		break;
	case IDLN_NONE:
		g_error("objIDL doesn't support um.. nones yet");		
		g_assert(false);
		break;
	case IDLN_OP_DCL:
		return new IDLOpDcl(tree);
		break;
	case IDLN_PARAM_DCL:
		return new IDLParamDcl(tree);
		break;
	case IDLN_STRING:
		return new IDLPrimitiveType(tree);	
		break;
	case IDLN_TYPE_ANY:
		g_error("objIDL doesn't support anys yet");
		g_assert(false);
		break;
	case IDLN_TYPE_ARRAY:
		g_error("objIDL doesn't support arrays yet");
		g_assert(false);
		break;
	case IDLN_TYPE_BOOLEAN:
		g_error("objIDL doesn't support boolean types yet");				
		g_assert(false);
		break;
	case IDLN_TYPE_CHAR:
		g_error("objIDL doesn't support char types yet");		
		g_assert(false);
		break;
	case IDLN_TYPE_DCL:
		return new IDLTypeDcl(tree);
		g_assert(false);
		break;
	case IDLN_TYPE_ENUM:
		g_error("objIDL doesn't support enums yet");
		g_assert(false);
		break;
	case IDLN_TYPE_FIXED:
		g_error("objIDL doesn't support fixed types yet");
		g_assert(false);
		break;
	case IDLN_TYPE_FLOAT:
		return new IDLTypeFloat(tree);	
		break;
	case IDLN_TYPE_INTEGER:		
		return new IDLTypeInteger(tree);	
		break;
	case IDLN_TYPE_OBJECT:
		g_error("objIDL doesn't support objects yet");
		g_assert(false);
		break;
	case IDLN_TYPE_OCTET:
		g_error("objIDL doesn't support octets yet");
		g_assert(false);
		break;
	case IDLN_TYPE_SEQUENCE:
		return new IDLTypeSequence(tree);
		break;
	case IDLN_TYPE_STRING:
		return new IDLPrimitiveType(tree);	
		break;
	case IDLN_TYPE_STRUCT:
		return new IDLStruct(tree);
		break;
	case IDLN_TYPE_UNION:
		return new IDLUnion(tree);
		break;
	case IDLN_TYPE_WIDE_CHAR:
		g_error("objIDL doesn't support widechars yet");
		g_assert(false);
		break;
	case IDLN_TYPE_WIDE_STRING:
		g_error("objIDL doesn't support widestrings yet");
		g_assert(false);
		break;
	case IDLN_UNARYOP:
		g_error("objIDL doesn't support unaryops yet");
		g_assert(false);
		break;
	case IDLN_WIDE_CHAR:
		g_error("objIDL doesn't support widechars yet");
		g_assert(false);
		break;
	case IDLN_WIDE_STRING:
		g_error("objIDL doesn't support widestrings yet");
		g_assert(false);
		break;
	case IDLN_ANY:
		g_error("objIDL doesn't support anys yet");
		g_assert(false);
		break;
	case IDLN_NATIVE:
		g_assert(false);
		break;
	default:
		g_error("IDLTree::tree_factory - couldn't deduce type of idl_tree");
	}
	
}

IDLTree::IDLTree()
{
	g_warning("IDLTree default constuctor called\n");
	m_tree = NULL;
	no_objects++;

}

IDLTree::IDLTree(IDL_tree tree)
{
	//	cout << "IDLTree constructor\n";
	//	cout << "Tree = " << tree << endl;
	m_tree = tree;
	no_objects++;
}

IDLTree::IDLTree(const IDLTree& tree)
{
	//	cout << "IDLTree copy constructor\n";
	m_tree = tree.m_tree;
	no_objects++;
}

IDLTree::~IDLTree()
{
	//	cout << "IDLTree destructor\n";
	
	no_objects--;
}


int IDLTree::get_type()
{
	g_assert(this);
	//	cerr << "IDLTree - get_type called\n";
	return IDL_NODE_TYPE(m_tree);
}


bool IDLTree::is_null()
{
	if(m_tree == NULL){
		return true;
	} else {
		return false;
	}
}

////////////////////// List ////////////////



IDLList::IDLList(IDL_tree t)
	: IDLTree(t)
{
}

IDLList*
IDLList::next()
{
	// cout << "IDLLIst::next called\n";
	IDL_tree next = IDL_LIST(m_tree).next;
	IDLList* nextItem = NULL;
	
	if(next){
		nextItem = new IDLList(next);
	}

	return nextItem;
}

IDLTree*
IDLList::get_tree()
{
	g_assert(IDL_LIST(m_tree).data != 0);
	return tree_factory(IDL_LIST(m_tree).data);
}



////////////////////// Int ////////////////



IDLTypeInteger::IDLTypeInteger(IDL_tree t)
	: IDLTree(t)
{
}


bool
IDLTypeInteger::is_signed()
{
	if(IDL_TYPE_INTEGER(m_tree).f_signed)
		return true;
	else
		return false;
}

IDLTypeInteger::bitwidth
IDLTypeInteger::get_width()
{
	switch(IDL_TYPE_INTEGER(m_tree).f_type) {
	case IDL_INTEGER_TYPE_SHORT:
		return SHORT;
		break;
	case IDL_INTEGER_TYPE_LONGLONG:
		return LONGLONG;
	case IDL_INTEGER_TYPE_LONG:
		return LONG;
		break;
	}
}
////////////////////// Int ////////////////



IDLTypeSequence::IDLTypeSequence(IDL_tree t)
	: IDLTree(t)
{
}

IDLTree*
IDLTypeSequence::get_simple_typespec()
{
	IDL_tree type = IDL_TYPE_SEQUENCE(m_tree).simple_type_spec;
	return tree_factory(type);	
}

int
IDLTypeSequence::get_size()
{
	int size;
	IDL_tree tree = IDL_TYPE_SEQUENCE(m_tree).positive_int_const;
	if(tree == NULL){
		size = 0;
	} else {
		size = IDL_INTEGER(tree).value;
	}
	return size;
}

/////////////////////// Floats ///////////////////////////////////

IDLTypeFloat::IDLTypeFloat(IDL_tree t)
	: IDLTree(t)
{
}

IDLTypeFloat::floattype
IDLTypeFloat::get_floattype()
{
	switch(IDL_TYPE_FLOAT(m_tree).f_type) {
	case IDL_FLOAT_TYPE_FLOAT:
		return FLOAT;
		break;
	case IDL_FLOAT_TYPE_DOUBLE:
		return DOUBLE;
	case IDL_FLOAT_TYPE_LONGDOUBLE:
		return LONGDOUBLE;
		break;
	}
}


/////////////////////// IDLIdent //////////////////////////////////  

IDLIdent::IDLIdent(IDL_tree t)
	: IDLTree(t)
{
}

const char*
IDLIdent::get_name(int indent)
{
	char* name = IDL_ns_ident_to_qstring(IDL_IDENT_TO_NS(m_tree),
										 m_separator, indent);
	g_assert(name != 0);	
	return name;
}


const char*
IDLIdent::get_repoid()
{
	char* name = IDL_IDENT_REPO_ID(m_tree);
	g_assert(name != 0);
	
	return name;
}


/////////////////////// IDLTypeDcl //////////////////////////////////  

IDLTypeDcl::IDLTypeDcl(IDL_tree t)
	: IDLTree(t)
{
}


IDLTree*
IDLTypeDcl::get_typespec()
{
	IDL_tree type = IDL_TYPE_DCL(m_tree).type_spec;
	return tree_factory(type);
}

IDLList*
IDLTypeDcl::get_dcls()
{
	return new IDLList(IDL_TYPE_DCL(m_tree).dcls);
}




IDLConstDcl::IDLConstDcl(IDL_tree t)
	: IDLTree(t)
{
}


/////////////////////// IDLExceptDcl //////////////////////////////////  


IDLExceptDcl::IDLExceptDcl(IDL_tree t)
	: IDLTree(t)
{
}

const char*
IDLExceptDcl::get_name(int indent)
{
	char* name = IDL_ns_ident_to_qstring(IDL_IDENT_TO_NS(IDL_EXCEPT_DCL(m_tree).ident),
										 m_separator, indent);
	g_assert(name != 0);
	
	return name;
}

IDLList*
IDLExceptDcl::get_contents()
{
	return new IDLList(IDL_EXCEPT_DCL(m_tree).members);
}

/////////////////////// IDLStruct //////////////////////////////////  


IDLStruct::IDLStruct(IDL_tree t)
	: IDLTree(t)
{
}

const char*
IDLStruct::get_name(int indent)
{
	char* name = IDL_ns_ident_to_qstring(IDL_IDENT_TO_NS(IDL_TYPE_STRUCT(m_tree).ident),
										 m_separator, indent);
	g_assert(name != 0);
	
	return name;
}

const char*
IDLStruct::get_repoid()
{
	char* name = IDL_IDENT_REPO_ID(IDL_TYPE_STRUCT(m_tree).ident);
	g_assert(name != 0);
	
	return name;
}


IDLList*
IDLStruct::get_contents()
{
	return new IDLList(IDL_TYPE_STRUCT(m_tree).member_list);
}

/////////////////////// IDLUnion //////////////////////////////////  


IDLUnion::IDLUnion(IDL_tree t)
	: IDLTree(t)
{
}

const char*
IDLUnion::get_name(int indent)
{
	char* name = IDL_ns_ident_to_qstring(IDL_IDENT_TO_NS(IDL_TYPE_UNION(m_tree).ident),
										 m_separator, indent);
	g_assert(name != 0);
	
	return name;
}

const char*
IDLUnion::get_repoid()
{
	char* name = IDL_IDENT_REPO_ID(IDL_TYPE_UNION(m_tree).ident);
	g_assert(name != 0);
	
	return name;
}

IDLTree* 
IDLUnion::get_switch_typespec()
{
	IDL_tree tree;
	tree = IDL_TYPE_UNION(m_tree).switch_type_spec;	
	IDLTree* t = tree_factory(tree);
	return t;

}

IDLList* 
IDLUnion::get_switch_body()
{
	return new IDLList(IDL_TYPE_UNION(m_tree).switch_body);	
}


//////////////////////// AttrDcl //////////////////////////


IDLAttrDcl::IDLAttrDcl(IDL_tree t)
	: IDLTree(t)
{
}


//////////////////////// Param //////////////////////////

IDLParamDcl::IDLParamDcl(IDL_tree t)
	: IDLTree(t)
{
}

int
IDLParamDcl::get_attr() 
{
	int attr = IDL_PARAM_DCL(m_tree).attr;
	/*
	  switch(attr){
	  case IDL_PARAM_IN:
	  return IN;
	  case IDL_PARAM_OUT:
	  return OUT;
	  case IDL_PARAM_INOUT:
	  return INOUT;
	  default:
	  g_error("get_attr - type wasn't in, out or inout");
	  }
	*/
	return attr;
}

IDLTree*
IDLParamDcl::get_typespec()
{
	IDL_tree type = IDL_PARAM_DCL(m_tree).param_type_spec;
	return tree_factory(type);
}

const char*
IDLParamDcl::get_name()
{
	return IDL_IDENT(IDL_PARAM_DCL(m_tree).simple_declarator).str;
}


/////////////////////// Case Stmt //////////////////////////////////  

	
IDLCaseStmt::IDLCaseStmt(IDL_tree t)
	: IDLTree(t)
{
}


//////////////////////////////// Interface //////////////////////


IDLInterface::IDLInterface(IDL_tree t)
	: IDLTree(t)
{
}

const char*
IDLInterface::get_name(int indent)
{
	char* name = IDL_ns_ident_to_qstring(IDL_IDENT_TO_NS(IDL_INTERFACE(m_tree).ident),
										 m_separator, indent);
	g_assert(name != 0);
	
	return name;
}

const char*
IDLInterface::get_repoid()
{
	char* name = IDL_IDENT_REPO_ID(IDL_INTERFACE(m_tree).ident);
	g_assert(name != 0);
	
	return name;
}


IDLList*
IDLInterface::get_contents()
{
	//	cout << "interface::get_contents()\n";
	return new IDLList(IDL_INTERFACE(m_tree).body);
}

IDLList*
IDLInterface::get_inheritance_spec()
{

	IDLList* inheritance_spec = NULL;
	if(IDL_INTERFACE(m_tree).inheritance_spec){
		inheritance_spec =
			new IDLList(IDL_INTERFACE(m_tree).inheritance_spec);
	}
	return inheritance_spec;
}


//////////////////// ForwardDcl ///////////////////////////

IDLForwardDcl::IDLForwardDcl(IDL_tree t)
	: IDLTree(t)
{
}

//////////////////// Module ///////////////////////////////

IDLModule::IDLModule(IDL_tree t)
	: IDLTree(t)
{
}


const char*
IDLModule::get_name(int indent)
{
	char* name = IDL_ns_ident_to_qstring(IDL_IDENT_TO_NS(IDL_MODULE(m_tree).ident),
										 m_separator, indent);
	g_assert(name != 0);
	
	return name;
}

IDLList*
IDLModule::get_contents()
{
	return new IDLList(IDL_MODULE(m_tree).definition_list);
}

//////////////////// Member ///////////////////////////////

IDLMember::IDLMember(IDL_tree t)
	: IDLTree(t)
{
}

IDLTree*
IDLMember::get_typespec()
{
	IDL_tree type = IDL_MEMBER(m_tree).type_spec;
	return tree_factory(type);
}

IDLList*
IDLMember::get_dcls()
{
	return new IDLList(IDL_MEMBER(m_tree).dcls);
}


//////////////////// OpDcl ///////////////////////////////

IDLOpDcl::IDLOpDcl(IDL_tree t)
	: IDLTree(t)
{
}

const char*
IDLOpDcl::get_name(int indent)
{
	char* name = IDL_ns_ident_to_qstring(IDL_IDENT_TO_NS(IDL_OP_DCL(m_tree).ident),
										 m_separator, indent);
	g_assert(name != 0);
	
	return name;
}

IDLList*
IDLOpDcl::get_arguments()
{
	IDLList* args = NULL;
	if(IDL_OP_DCL(m_tree).parameter_dcls){
		args = new IDLList(IDL_OP_DCL(m_tree).parameter_dcls);
	}
	return args;
}

IDLList*
IDLOpDcl::get_exceptions()
{
	IDLList* exceptions = NULL;
	if(IDL_OP_DCL(m_tree).raises_expr){
		exceptions = new IDLList(IDL_OP_DCL(m_tree).raises_expr);
	}
	return exceptions;
}

IDLTree*
IDLOpDcl::get_return_typespec()
{
	IDL_tree tree = IDL_OP_DCL(m_tree).op_type_spec;
	
	//	g_assert(tree != 0);
	if(tree == 0) {
		return NULL;
	} else {
		return tree_factory(tree);
	}
}


IDLPrimitiveType::IDLPrimitiveType(IDL_tree t)
	: IDLTree(t)
{
}

