from objIDL import *


class NULL_IDLList:
	""" A null list (duh) """
	def accept(self, visitor):
		visitor.do_list(self)

	def __getitem__(self, index):
		raise IndexError

	def __len__(self):
		return 0


class IDLList2(IDLList):
	
	def __init__(self,rawtree):
		IDLList.__init__(self,rawtree)
		
	def accept(self, visitor):
		visitor.do_list(self)

	def __getitem__(self, index):
		#recurse through the list structure and return the appropriate tree
		list = self
		while index > 0 and list != "NULL":
			list = list.next()
			index = index-1
			
		if list != "NULL":
			return list.get_tree()
		else:
			raise IndexError
	
	def get_tree(self):
		tree = IDLList.get_tree(self)
		return cast_tree(tree)

	def next(self):
		""" Overridden so that null pointers get forwarded correctly"""
		val = objIDLc.IDLList_next(self.this)
		if val == "NULL":
			return val
		val = IDLListPtr(val)
		val.thisown = 1
		return IDLList2(val.get_raw_IDL_tree())

	def __len__(self):
		len=0
		for i in self:
			len=len+1
		return len







class IDLModule2(IDLModule):

	def accept(self, visitor):
		visitor.do_module(self)

	def get_contents(self):
		tree = IDLModule.get_contents(self)
		return IDLList2(tree.get_raw_IDL_tree())


class IDLStruct2(IDLStruct):

	def accept(self, visitor):
		visitor.do_struct(self)

	def get_contents(self):
		tree = IDLStruct.get_contents(self)
		return IDLList2(tree.get_raw_IDL_tree())


class IDLUnion2(IDLUnion):

	def accept(self, visitor):
		visitor.do_union(self)

	def get_switch_typespec(self):
		tree = IDLUnion.get_switch_typespec(self)
		return cast_tree(tree)

	def get_switch_body(self):
		tree = IDLUnion.get_switch_body(self)
		return IDLList2(tree.get_raw_IDL_tree())



class IDLInterface2(IDLInterface):

	def accept(self, visitor):
		visitor.do_interface(self)
		
	def get_contents(self):
		tree = IDLInterface.get_contents(self)
		return IDLList2(tree.get_raw_IDL_tree())

	def get_inheritance_spec(self):
		val = objIDLc.IDLInterface_get_inheritance_spec(self.this)
		if val == "NULL":
			return NULL_IDLList()
		val = IDLListPtr(val)
		val.thisown = 1
		return IDLList2(val.get_raw_IDL_tree())











class IDLOpDcl2(IDLOpDcl):

	def accept(self, visitor):
		visitor.do_opDcl(self)

	def get_return_typespec(self):
		""" Overridden so that null pointers get forwarded correctly"""
		tree = objIDLc.IDLOpDcl_get_return_typespec(self.this)
		if tree == "NULL":
			return IDLTypeVoid()
		val = IDLTreePtr(tree)
		val.thisown = 1
		return cast_tree(val)
		
	def get_arguments(self):
		""" Overridden so that null pointers get forwarded correctly"""
#		print "get_arguments called"
		val = objIDLc.IDLOpDcl_get_arguments(self.this)
		if val == "NULL":
			return NULL_IDLList()
		val = IDLListPtr(val)
		val.thisown = 1
		return IDLList2(val.get_raw_IDL_tree())

	def get_exceptions(self):
		""" Overridden so that null pointers get forwarded correctly"""
#		print "get_exceptions called"
		val = objIDLc.IDLOpDcl_get_exceptions(self.this)
		if val == "NULL":
			return NULL_IDLList()
		val = IDLListPtr(val)
		val.thisown = 1
		return IDLList2(val.get_raw_IDL_tree())

class IDLParamDcl2(IDLParamDcl):
	
	def accept(self, visitor):
		visitor.do_paramDcl(self)

	def get_typespec(self):
		tree = IDLParamDcl.get_typespec(self)
		return cast_tree(tree)

class IDLExceptDcl2(IDLExceptDcl):
	def accept(self,visitor):
		visitor.do_exceptDcl(self)
		
	def get_contents(self):
		tree = IDLExceptDcl.get_contents(self)
		return IDLList2(tree.get_raw_IDL_tree())
	
class IDLIdent2(IDLIdent):
	def accept(self, visitor):
		visitor.do_ident(self)

class IDLTypeDcl2(IDLTypeDcl):
	def accept(self, visitor):
		visitor.do_type_dcl(self)

	def get_typespec(self):
		tree = IDLTypeDcl.get_typespec(self)
		return cast_tree(tree)

	def get_dcls(self):
		tree = IDLTypeDcl.get_dcls(self)
		return IDLList2(tree.get_raw_IDL_tree())


class IDLMember2(IDLMember):
	def accept(self, visitor):
		visitor.do_member(self)

	def get_typespec(self):
		tree = IDLMember.get_typespec(self)
		return cast_tree(tree)

	def get_dcls(self):
		tree = IDLMember.get_dcls(self)
		return IDLList2(tree.get_raw_IDL_tree())


class IDLTypeString2(IDLPrimitiveType):
	def accept(self, visitor):
		visitor.do_type_string(self)

class IDLTypeInteger2(IDLTypeInteger):
	def accept(self, visitor):
		visitor.do_type_integer(self)


class IDLTypeSequence2(IDLTypeSequence):
	def accept(self, visitor):
		visitor.do_type_sequence(self)

	def get_simple_typespec(self):
		tree = IDLTypeSequence.get_simple_typespec(self)
		return cast_tree(tree)


class IDLTypeFloat2(IDLTypeFloat):
	def accept(self, visitor):
		visitor.do_type_float(self)


class IDLTypeVoid:
	def accept(self, visitor):
		visitor.do_type_void()


def parse_file(name):
	tree = IDLTree_parse_file(name)
	return cast_tree(tree)

def set_separator(separator):
	IDLTree_set_separator(separator)

def cast_tree(tree):
	"""
	Hack to cast the c++ tree into the appropriate type. 
	This is because the c++ object will be of type tree when
	passed to python.
	"""
	
	if tree.get_type() == IDLN_NONE:
		raise "Error - tree is a none!\n"
	elif tree.get_type() == IDLN_LIST:
		return IDLList2(tree.get_raw_IDL_tree())	
	elif tree.get_type() == IDLN_MODULE:
		return IDLModule2(tree.get_raw_IDL_tree())	
	elif tree.get_type() == IDLN_INTERFACE:
		return IDLInterface2(tree.get_raw_IDL_tree())	
	elif tree.get_type() == IDLN_OP_DCL:
		return IDLOpDcl2(tree.get_raw_IDL_tree())	
	elif tree.get_type() == IDLN_PARAM_DCL:
		return IDLParamDcl2(tree.get_raw_IDL_tree())
	elif tree.get_type() == IDLN_TYPE_DCL:
		return IDLTypeDcl2(tree.get_raw_IDL_tree())
	elif tree.get_type() == IDLN_EXCEPT_DCL:
		return IDLExceptDcl2(tree.get_raw_IDL_tree())
	elif tree.get_type() == IDLN_MEMBER:
		return IDLMember2(tree.get_raw_IDL_tree())
	elif tree.get_type() == IDLN_IDENT:
		return IDLIdent2(tree.get_raw_IDL_tree())
	elif tree.get_type() == IDLN_TYPE_STRING:
		return IDLTypeString2(tree.get_raw_IDL_tree())
	elif tree.get_type() == IDLN_TYPE_STRUCT:
		return IDLStruct2(tree.get_raw_IDL_tree())	
	elif tree.get_type() == IDLN_TYPE_UNION:
		return IDLUnion2(tree.get_raw_IDL_tree())	
	elif tree.get_type() == IDLN_TYPE_INTEGER:
		return IDLTypeInteger2(tree.get_raw_IDL_tree())
	elif tree.get_type() == IDLN_TYPE_FLOAT:
		return IDLTypeFloat2(tree.get_raw_IDL_tree())
	elif tree.get_type() == IDLN_TYPE_SEQUENCE:
		return IDLTypeSequence2(tree.get_raw_IDL_tree())
	else:
		raise "Error - Couldn't deduce tree type!\n"

	
