# This file was created automatically by SWIG.
import objIDLc
class IDLTreePtr :
    def __init__(self,this):
        self.this = this
        self.thisown = 0
    def __del__(self):
        if self.thisown == 1 :
            objIDLc.delete_IDLTree(self.this)
    def get_type(self):
        val = objIDLc.IDLTree_get_type(self.this)
        return val
    def get_raw_IDL_tree(self):
        val = objIDLc.IDLTree_get_raw_IDL_tree(self.this)
        return val
    def __repr__(self):
        return "<C IDLTree instance>"
class IDLTree(IDLTreePtr):
    def __init__(self,this):
        self.this = this




class IDLListPtr(IDLTreePtr):
    def __init__(self,this):
        self.this = this
        self.thisown = 0
    def next(self):
        val = objIDLc.IDLList_next(self.this)
        val = IDLListPtr(val)
        val.thisown = 1
        return val
    def get_tree(self):
        val = objIDLc.IDLList_get_tree(self.this)
        val = IDLTreePtr(val)
        val.thisown = 1
        return val
    def __repr__(self):
        return "<C IDLList instance>"
class IDLList(IDLListPtr):
    def __init__(self,arg0) :
        self.this = objIDLc.new_IDLList(arg0)
        self.thisown = 1




class IDLModulePtr(IDLTreePtr):
    def __init__(self,this):
        self.this = this
        self.thisown = 0
    def get_name(self,arg0):
        val = objIDLc.IDLModule_get_name(self.this,arg0)
        return val
    def get_contents(self):
        val = objIDLc.IDLModule_get_contents(self.this)
        val = IDLListPtr(val)
        val.thisown = 1
        return val
    def __repr__(self):
        return "<C IDLModule instance>"
class IDLModule(IDLModulePtr):
    def __init__(self,arg0) :
        self.this = objIDLc.new_IDLModule(arg0)
        self.thisown = 1




class IDLStructPtr(IDLTreePtr):
    def __init__(self,this):
        self.this = this
        self.thisown = 0
    def get_name(self,arg0):
        val = objIDLc.IDLStruct_get_name(self.this,arg0)
        return val
    def get_contents(self):
        val = objIDLc.IDLStruct_get_contents(self.this)
        val = IDLListPtr(val)
        val.thisown = 1
        return val
    def get_repoid(self):
        val = objIDLc.IDLStruct_get_repoid(self.this)
        return val
    def __repr__(self):
        return "<C IDLStruct instance>"
class IDLStruct(IDLStructPtr):
    def __init__(self,arg0) :
        self.this = objIDLc.new_IDLStruct(arg0)
        self.thisown = 1




class IDLUnionPtr(IDLTreePtr):
    def __init__(self,this):
        self.this = this
        self.thisown = 0
    def get_name(self,arg0):
        val = objIDLc.IDLUnion_get_name(self.this,arg0)
        return val
    def get_repoid(self):
        val = objIDLc.IDLUnion_get_repoid(self.this)
        return val
    def get_switch_typespec(self):
        val = objIDLc.IDLUnion_get_switch_typespec(self.this)
        val = IDLTreePtr(val)
        val.thisown = 1
        return val
    def get_switch_body(self):
        val = objIDLc.IDLUnion_get_switch_body(self.this)
        val = IDLListPtr(val)
        val.thisown = 1
        return val
    def __repr__(self):
        return "<C IDLUnion instance>"
class IDLUnion(IDLUnionPtr):
    def __init__(self,arg0) :
        self.this = objIDLc.new_IDLUnion(arg0)
        self.thisown = 1




class IDLInterfacePtr(IDLTreePtr):
    def __init__(self,this):
        self.this = this
        self.thisown = 0
    def get_name(self,arg0):
        val = objIDLc.IDLInterface_get_name(self.this,arg0)
        return val
    def get_repoid(self):
        val = objIDLc.IDLInterface_get_repoid(self.this)
        return val
    def get_contents(self):
        val = objIDLc.IDLInterface_get_contents(self.this)
        val = IDLListPtr(val)
        val.thisown = 1
        return val
    def get_inheritance_spec(self):
        val = objIDLc.IDLInterface_get_inheritance_spec(self.this)
        val = IDLListPtr(val)
        val.thisown = 1
        return val
    def __repr__(self):
        return "<C IDLInterface instance>"
class IDLInterface(IDLInterfacePtr):
    def __init__(self,arg0) :
        self.this = objIDLc.new_IDLInterface(arg0)
        self.thisown = 1




class IDLMemberPtr(IDLTreePtr):
    def __init__(self,this):
        self.this = this
        self.thisown = 0
    def get_typespec(self):
        val = objIDLc.IDLMember_get_typespec(self.this)
        val = IDLTreePtr(val)
        val.thisown = 1
        return val
    def get_dcls(self):
        val = objIDLc.IDLMember_get_dcls(self.this)
        val = IDLListPtr(val)
        val.thisown = 1
        return val
    def __repr__(self):
        return "<C IDLMember instance>"
class IDLMember(IDLMemberPtr):
    def __init__(self,arg0) :
        self.this = objIDLc.new_IDLMember(arg0)
        self.thisown = 1




class IDLTypeDclPtr(IDLTreePtr):
    def __init__(self,this):
        self.this = this
        self.thisown = 0
    def get_typespec(self):
        val = objIDLc.IDLTypeDcl_get_typespec(self.this)
        val = IDLTreePtr(val)
        val.thisown = 1
        return val
    def get_dcls(self):
        val = objIDLc.IDLTypeDcl_get_dcls(self.this)
        val = IDLListPtr(val)
        val.thisown = 1
        return val
    def __repr__(self):
        return "<C IDLTypeDcl instance>"
class IDLTypeDcl(IDLTypeDclPtr):
    def __init__(self,arg0) :
        self.this = objIDLc.new_IDLTypeDcl(arg0)
        self.thisown = 1




class IDLTypeSequencePtr(IDLTreePtr):
    def __init__(self,this):
        self.this = this
        self.thisown = 0
    def get_simple_typespec(self):
        val = objIDLc.IDLTypeSequence_get_simple_typespec(self.this)
        val = IDLTreePtr(val)
        val.thisown = 1
        return val
    def get_size(self):
        val = objIDLc.IDLTypeSequence_get_size(self.this)
        return val
    def __repr__(self):
        return "<C IDLTypeSequence instance>"
class IDLTypeSequence(IDLTypeSequencePtr):
    def __init__(self,arg0) :
        self.this = objIDLc.new_IDLTypeSequence(arg0)
        self.thisown = 1




class IDLIdentPtr(IDLTreePtr):
    def __init__(self,this):
        self.this = this
        self.thisown = 0
    def get_name(self,arg0):
        val = objIDLc.IDLIdent_get_name(self.this,arg0)
        return val
    def get_repoid(self):
        val = objIDLc.IDLIdent_get_repoid(self.this)
        return val
    def __repr__(self):
        return "<C IDLIdent instance>"
class IDLIdent(IDLIdentPtr):
    def __init__(self,arg0) :
        self.this = objIDLc.new_IDLIdent(arg0)
        self.thisown = 1




class IDLOpDclPtr(IDLTreePtr):
    def __init__(self,this):
        self.this = this
        self.thisown = 0
    def get_name(self,arg0):
        val = objIDLc.IDLOpDcl_get_name(self.this,arg0)
        return val
    def get_arguments(self):
        val = objIDLc.IDLOpDcl_get_arguments(self.this)
        val = IDLListPtr(val)
        val.thisown = 1
        return val
    def get_exceptions(self):
        val = objIDLc.IDLOpDcl_get_exceptions(self.this)
        val = IDLListPtr(val)
        val.thisown = 1
        return val
    def get_return_typespec(self):
        val = objIDLc.IDLOpDcl_get_return_typespec(self.this)
        val = IDLTreePtr(val)
        val.thisown = 1
        return val
    def __repr__(self):
        return "<C IDLOpDcl instance>"
class IDLOpDcl(IDLOpDclPtr):
    def __init__(self,arg0) :
        self.this = objIDLc.new_IDLOpDcl(arg0)
        self.thisown = 1




class IDLParamDclPtr(IDLTreePtr):
    def __init__(self,this):
        self.this = this
        self.thisown = 0
    def get_attr(self):
        val = objIDLc.IDLParamDcl_get_attr(self.this)
        return val
    def get_typespec(self):
        val = objIDLc.IDLParamDcl_get_typespec(self.this)
        val = IDLTreePtr(val)
        val.thisown = 1
        return val
    def get_name(self):
        val = objIDLc.IDLParamDcl_get_name(self.this)
        return val
    def __repr__(self):
        return "<C IDLParamDcl instance>"
class IDLParamDcl(IDLParamDclPtr):
    def __init__(self,arg0) :
        self.this = objIDLc.new_IDLParamDcl(arg0)
        self.thisown = 1




class IDLExceptDclPtr(IDLTreePtr):
    def __init__(self,this):
        self.this = this
        self.thisown = 0
    def get_name(self,arg0):
        val = objIDLc.IDLExceptDcl_get_name(self.this,arg0)
        return val
    def get_contents(self):
        val = objIDLc.IDLExceptDcl_get_contents(self.this)
        val = IDLListPtr(val)
        val.thisown = 1
        return val
    def __repr__(self):
        return "<C IDLExceptDcl instance>"
class IDLExceptDcl(IDLExceptDclPtr):
    def __init__(self,arg0) :
        self.this = objIDLc.new_IDLExceptDcl(arg0)
        self.thisown = 1




class IDLPrimitiveTypePtr(IDLTreePtr):
    def __init__(self,this):
        self.this = this
        self.thisown = 0
    def __repr__(self):
        return "<C IDLPrimitiveType instance>"
class IDLPrimitiveType(IDLPrimitiveTypePtr):
    def __init__(self,arg0) :
        self.this = objIDLc.new_IDLPrimitiveType(arg0)
        self.thisown = 1




class IDLTypeIntegerPtr(IDLTreePtr):
    SHORT = objIDLc.IDLTypeInteger_SHORT
    LONG = objIDLc.IDLTypeInteger_LONG
    LONGLONG = objIDLc.IDLTypeInteger_LONGLONG
    def __init__(self,this):
        self.this = this
        self.thisown = 0
    def get_width(self):
        val = objIDLc.IDLTypeInteger_get_width(self.this)
        return val
    def is_signed(self):
        val = objIDLc.IDLTypeInteger_is_signed(self.this)
        return val
    def __repr__(self):
        return "<C IDLTypeInteger instance>"
class IDLTypeInteger(IDLTypeIntegerPtr):
    def __init__(self,arg0) :
        self.this = objIDLc.new_IDLTypeInteger(arg0)
        self.thisown = 1




class IDLTypeFloatPtr(IDLTreePtr):
    FLOAT = objIDLc.IDLTypeFloat_FLOAT
    DOUBLE = objIDLc.IDLTypeFloat_DOUBLE
    LONGDOUBLE = objIDLc.IDLTypeFloat_LONGDOUBLE
    def __init__(self,this):
        self.this = this
        self.thisown = 0
    def get_floattype(self):
        val = objIDLc.IDLTypeFloat_get_floattype(self.this)
        return val
    def __repr__(self):
        return "<C IDLTypeFloat instance>"
class IDLTypeFloat(IDLTypeFloatPtr):
    def __init__(self,arg0) :
        self.this = objIDLc.new_IDLTypeFloat(arg0)
        self.thisown = 1






#-------------- FUNCTION WRAPPERS ------------------

def IDLTree_parse_file(arg0):
    val = objIDLc.IDLTree_parse_file(arg0)
    val = IDLTreePtr(val)
    val.thisown = 1
    return val

IDLTree_set_separator = objIDLc.IDLTree_set_separator

IDLTree_get_no_objects = objIDLc.IDLTree_get_no_objects



#-------------- VARIABLE WRAPPERS ------------------

IDLN_NONE = objIDLc.IDLN_NONE
IDLN_ANY = objIDLc.IDLN_ANY
IDLN_LIST = objIDLc.IDLN_LIST
IDLN_GENTREE = objIDLc.IDLN_GENTREE
IDLN_INTEGER = objIDLc.IDLN_INTEGER
IDLN_STRING = objIDLc.IDLN_STRING
IDLN_WIDE_STRING = objIDLc.IDLN_WIDE_STRING
IDLN_CHAR = objIDLc.IDLN_CHAR
IDLN_WIDE_CHAR = objIDLc.IDLN_WIDE_CHAR
IDLN_FIXED = objIDLc.IDLN_FIXED
IDLN_FLOAT = objIDLc.IDLN_FLOAT
IDLN_BOOLEAN = objIDLc.IDLN_BOOLEAN
IDLN_IDENT = objIDLc.IDLN_IDENT
IDLN_TYPE_DCL = objIDLc.IDLN_TYPE_DCL
IDLN_CONST_DCL = objIDLc.IDLN_CONST_DCL
IDLN_EXCEPT_DCL = objIDLc.IDLN_EXCEPT_DCL
IDLN_ATTR_DCL = objIDLc.IDLN_ATTR_DCL
IDLN_OP_DCL = objIDLc.IDLN_OP_DCL
IDLN_PARAM_DCL = objIDLc.IDLN_PARAM_DCL
IDLN_FORWARD_DCL = objIDLc.IDLN_FORWARD_DCL
IDLN_TYPE_INTEGER = objIDLc.IDLN_TYPE_INTEGER
IDLN_TYPE_FLOAT = objIDLc.IDLN_TYPE_FLOAT
IDLN_TYPE_FIXED = objIDLc.IDLN_TYPE_FIXED
IDLN_TYPE_CHAR = objIDLc.IDLN_TYPE_CHAR
IDLN_TYPE_WIDE_CHAR = objIDLc.IDLN_TYPE_WIDE_CHAR
IDLN_TYPE_STRING = objIDLc.IDLN_TYPE_STRING
IDLN_TYPE_WIDE_STRING = objIDLc.IDLN_TYPE_WIDE_STRING
IDLN_TYPE_BOOLEAN = objIDLc.IDLN_TYPE_BOOLEAN
IDLN_TYPE_OCTET = objIDLc.IDLN_TYPE_OCTET
IDLN_TYPE_ANY = objIDLc.IDLN_TYPE_ANY
IDLN_TYPE_OBJECT = objIDLc.IDLN_TYPE_OBJECT
IDLN_TYPE_TYPECODE = objIDLc.IDLN_TYPE_TYPECODE
IDLN_TYPE_ENUM = objIDLc.IDLN_TYPE_ENUM
IDLN_TYPE_SEQUENCE = objIDLc.IDLN_TYPE_SEQUENCE
IDLN_TYPE_ARRAY = objIDLc.IDLN_TYPE_ARRAY
IDLN_TYPE_STRUCT = objIDLc.IDLN_TYPE_STRUCT
IDLN_TYPE_UNION = objIDLc.IDLN_TYPE_UNION
IDLN_MEMBER = objIDLc.IDLN_MEMBER
IDLN_NATIVE = objIDLc.IDLN_NATIVE
IDLN_CASE_STMT = objIDLc.IDLN_CASE_STMT
IDLN_INTERFACE = objIDLc.IDLN_INTERFACE
IDLN_MODULE = objIDLc.IDLN_MODULE
IDLN_BINOP = objIDLc.IDLN_BINOP
IDLN_UNARYOP = objIDLc.IDLN_UNARYOP
IDLN_CODEFRAG = objIDLc.IDLN_CODEFRAG
IDL_PARAM_IN = objIDLc.IDL_PARAM_IN
IDL_PARAM_OUT = objIDLc.IDL_PARAM_OUT
IDL_PARAM_INOUT = objIDLc.IDL_PARAM_INOUT
