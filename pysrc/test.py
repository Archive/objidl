#!/usr/bin/python

from pyIDL import *

import sys
			
##########################################


#a = idl.parse_file("echo.idl")

class treeTester:

	def __init__(self):
		self.indent =0
		self.tabs=0


	def printtabs(self):
		for i in range(self.tabs):
			print "\t",
	
	def do_list(self,list):
#		print "list"
		for tree in list:
			tree.accept(self)
			
			
	def do_module(self,module):
		self.printtabs()
		print "module: ",self.indent,module.get_name(self.indent)
		self.indent = self.indent +1
		self.tabs = self.tabs +1
		self.do_list(module.get_contents())
		self.tabs = self.tabs -1
		self.indent = self.indent -1

	def do_interface(self,interface):
		self.printtabs()
		print "interface: "+ interface.get_name(1)
		print "interface repoid: "+ interface.get_repoid()

		print "inherits: ",
		self.tabs = self.tabs +1
		self.do_list(interface.get_inheritance_spec())
		print ""
		self.tabs = self.tabs -1

		print "contents:"
		self.tabs = self.tabs +1
		self.do_list(interface.get_contents())
		self.tabs = self.tabs -1

	def do_opDcl(self,op):
		self.printtabs()
		print "operation: " + op.get_name(1)
		self.tabs = self.tabs +1
		returntype = op.get_return_typespec()
		self.printtabs()
		print "return type:",
		returntype.accept(self)
		print ""
		self.do_list(op.get_arguments())
		self.printtabs()
		print "raises:",
		self.do_list(op.get_exceptions())
		print ""



		self.tabs = self.tabs -1
		

	def do_paramDcl(self,param):
		self.printtabs()
		print "param: name=",param.get_name()," type = ",
		param.get_typespec().accept(self)
		print ""


	def do_exceptDcl(self,ex):
		self.printtabs()
		print "exception: name=",ex.get_name(self.indent)
		self.tabs = self.tabs +1
		self.do_list(ex.get_contents())
		self.tabs = self.tabs -1

	def do_struct(self,s):
		self.printtabs()
		print "struct: " + s.get_name(1),
		print ", repoid= " + s.get_repoid()
		self.tabs = self.tabs +1
		for member in s.get_contents():
			member.accept(self)
		self.tabs = self.tabs -1

	def do_union(self,u):
		print "union name = ",u.get_name(0)
	
		print "switch_type = ",
		type = u.get_switch_typespec()
		type.accept(self)
		print 
		body = u.get_switch_body()
		body.accept(self)
				
		
	def do_type_dcl(self,d):
		print "type dcl"
		print "typespec=",
		type = d.get_typespec()
		type.accept(self)

		dcls = d.get_dcls()
		for dcl in dcls:
			dcl.accept(self)
		
	def do_type_sequence(self,s):
		print "sequence  ",
		print "simple typespec=",
		t = s.get_simple_typespec()
		t.accept(self)
		print "size=",s.get_size()

	def do_member(self,m):
		self.printtabs()
		type = m.get_typespec()
		dcls = m.get_dcls()
		print "Member: Type=",
		type.accept(self)
		self.tabs = self.tabs +1
		print "Dcls=",
		for dcl in dcls:
			dcl.accept(self)
		print ""
		self.tabs = self.tabs -1
		
	def do_ident(self, ident):
		print ident.get_name(self.indent),
		print ",repoid = ",ident.get_repoid(),

	def do_type_string(self, str):
		print "string",

	def do_type_integer(self, integer):
		print "integer: ",
		width = integer.get_width()
		if width == integer.SHORT:
			print "SHORT",
		if width == integer.LONG:
			print "LONG",
		if width == integer.LONGLONG:				
			print "LONGLONG",

	def do_type_float(self, float):
		print "float: ",
		width = float.get_floattype()
		if width == float.FLOAT:
			print "FLOAT",
		if width == float.DOUBLE:
			print "DOUBLE",
		if width == float.LONGDOUBLE:				
			print "LONGDOUBLE",


	def do_type_void(self):
		print "void",
	

def main():
	a = parse_file(sys.argv[1])
	visitor = treeTester()
	set_separator("::")
	a.accept(visitor)
	set_separator("_")
	#a.accept(visitor)

main()
print IDLTree_get_no_objects()


