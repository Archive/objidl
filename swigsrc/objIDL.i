%module objIDL
%{
#include "cppIDL.hh"

#include <iostream>

%}

typedef enum {
	IDLN_NONE,
	IDLN_ANY,

	IDLN_LIST,
	IDLN_GENTREE,
	IDLN_INTEGER,
	IDLN_STRING,
	IDLN_WIDE_STRING,
	IDLN_CHAR,
	IDLN_WIDE_CHAR,
	IDLN_FIXED,
	IDLN_FLOAT,
	IDLN_BOOLEAN,
	IDLN_IDENT,
	IDLN_TYPE_DCL,
	IDLN_CONST_DCL,
	IDLN_EXCEPT_DCL,
	IDLN_ATTR_DCL,
	IDLN_OP_DCL,
	IDLN_PARAM_DCL,
	IDLN_FORWARD_DCL,
	IDLN_TYPE_INTEGER,
	IDLN_TYPE_FLOAT,
	IDLN_TYPE_FIXED,
	IDLN_TYPE_CHAR,
	IDLN_TYPE_WIDE_CHAR,
	IDLN_TYPE_STRING,
	IDLN_TYPE_WIDE_STRING,
	IDLN_TYPE_BOOLEAN,
	IDLN_TYPE_OCTET,
	IDLN_TYPE_ANY,
	IDLN_TYPE_OBJECT,
	IDLN_TYPE_TYPECODE,
	IDLN_TYPE_ENUM,
	IDLN_TYPE_SEQUENCE,
	IDLN_TYPE_ARRAY,
	IDLN_TYPE_STRUCT,
	IDLN_TYPE_UNION,
	IDLN_MEMBER,
	IDLN_NATIVE,
	IDLN_CASE_STMT,
	IDLN_INTERFACE,
	IDLN_MODULE,
	IDLN_BINOP,
	IDLN_UNARYOP,
	IDLN_CODEFRAG,
} IDL_tree_type;


enum IDL_param_attr {
	IDL_PARAM_IN,
	IDL_PARAM_OUT,
	IDL_PARAM_INOUT
};


class IDLTree
{
public:
	~IDLTree();
	int get_type();

	IDL_tree get_raw_IDL_tree();
%new	static IDLTree* parse_file(const char* filename);
	static void set_separator(const char* m_seperator);

	static int get_no_objects();
};


class IDLList : public IDLTree
{
public:  
	IDLList(IDL_tree tree);
%new	IDLList* next();
%new	IDLTree* get_tree();
};


class IDLModule : public IDLTree
{
public:
	IDLModule(IDL_tree t);
%new	const char* get_name(int indent);
%new	IDLList* get_contents();
};


class IDLStruct : public IDLTree
{
public:
	IDLStruct(IDL_tree t);
%new	const char* get_name(int indent);
%new	IDLList* get_contents();
	const char* get_repoid();
};


class IDLUnion : public IDLTree
{
public:
	IDLUnion(IDL_tree t);
%new	const char* get_name(int indent);
	const char* get_repoid();
%new	IDLTree* get_switch_typespec();
%new	IDLList* get_switch_body();
};


class IDLInterface : public IDLTree
{
public:
	IDLInterface(IDL_tree t);
%new	const char*	get_name(int indent);
		const char*	get_repoid();
%new 	IDLList* get_contents();
%new 	IDLList* get_inheritance_spec();
};


class IDLMember : public IDLTree
{
public:
	IDLMember(IDL_tree t);
%new	IDLTree* get_typespec();
%new	IDLList* get_dcls();

};


class IDLTypeDcl : public IDLTree
{
public:
	IDLTypeDcl(IDL_tree t);
%new	IDLTree* get_typespec();
%new	IDLList* get_dcls();
}

class IDLTypeSequence : public IDLTree
{
public:
	IDLTypeSequence(IDL_tree t);
%new	IDLTree* get_simple_typespec();
	int get_size();
};


class IDLIdent : public IDLTree
{
public:
	IDLIdent(IDL_tree t);
%new	const char* get_name(int indent);
	const char* get_repoid();

};

class IDLOpDcl : public IDLTree
{
public:
	IDLOpDcl(IDL_tree t);
%new	const char* get_name(int indent);
%new	IDLList* get_arguments();
%new	IDLList* get_exceptions();
%new	IDLTree* get_return_typespec();

};


class IDLParamDcl : public IDLTree
{
public:
	IDLParamDcl(IDL_tree t);
		int get_attr(); 
%new	IDLTree* get_typespec();
		const char* get_name();

};

class IDLExceptDcl : public IDLTree
{
public:
	IDLExceptDcl(IDL_tree t);
%new	const char* get_name(int indent);
%new 	IDLList* get_contents();
};

class IDLPrimitiveType : public IDLTree
{
public:
	IDLPrimitiveType(IDL_tree t);	
};


class IDLTypeInteger : public IDLTree
{
public:
	enum bitwidth{
		SHORT,
		LONG,
		LONGLONG,
	};
  
public:
	IDLTypeInteger(IDL_tree t);
	bitwidth get_width();
	bool is_signed();
};


class IDLTypeFloat : public IDLTree
{
public:  
	enum floattype{
		FLOAT,
		DOUBLE,
		LONGDOUBLE,
	};
public:
	IDLTypeFloat(IDL_tree t);
	floattype get_floattype();
};
